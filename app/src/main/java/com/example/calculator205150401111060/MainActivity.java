package com.example.calculator205150401111060;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText et1, et2;
    Button btnTambah, btnKurang, btnKali, btnBagi;
    TextView tvHasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        et1= (EditText) findViewById(R.id.editText1);
        et2= (EditText) findViewById(R.id.editText2);
        tvHasil= (TextView) findViewById(R.id.textView4);
        btnTambah= (Button) findViewById(R.id.buttonTambah);
        btnKurang= (Button) findViewById(R.id.buttonKurang);
        btnKali= (Button) findViewById(R.id.buttonKali);
        btnBagi= (Button) findViewById(R.id.buttonBagi);

        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double bil1, bil2, hasil;
                bil1=Double.valueOf(et1.getText().toString().trim());
                bil2=Double.valueOf(et2.getText().toString().trim());
                hasil=bil1+bil2;
                String hasill=String.valueOf(hasil);
                tvHasil.setText(hasill);

                String bill1=String.valueOf(bil1);
                String bill2=String.valueOf(bil2);

                Intent intent = new Intent (view.getContext(), MainActivity2.class);
                intent.putExtra("bil1", bill1);
                intent.putExtra("bil2", bill2);
                intent.putExtra("hasil", hasill);
                intent.putExtra("operasi", "+");
                startActivity(intent);
            }
        });

        btnKurang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double bil1, bil2, hasil;
                bil1=Double.valueOf(et1.getText().toString().trim());
                bil2=Double.valueOf(et2.getText().toString().trim());
                hasil=bil1-bil2;
                String hasill=String.valueOf(hasil);
                tvHasil.setText(hasill);

                String bill1=String.valueOf(bil1);
                String bill2=String.valueOf(bil2);

                Intent intent = new Intent (view.getContext(), MainActivity2.class);
                intent.putExtra("bil1", bill1);
                intent.putExtra("bil2", bill2);
                intent.putExtra("hasil", hasill);
                intent.putExtra("operasi", "-");
                startActivity(intent);
            }
        });

        btnKali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double bil1, bil2, hasil;
                bil1=Double.valueOf(et1.getText().toString().trim());
                bil2=Double.valueOf(et2.getText().toString().trim());
                hasil=bil1*bil2;
                String hasill=String.valueOf(hasil);
                tvHasil.setText(hasill);

                String bill1=String.valueOf(bil1);
                String bill2=String.valueOf(bil2);

                Intent intent = new Intent (view.getContext(), MainActivity2.class);
                intent.putExtra("bil1", bill1);
                intent.putExtra("bil2", bill2);
                intent.putExtra("hasil", hasill);
                intent.putExtra("operasi", "x");
                startActivity(intent);
            }
        });

        btnBagi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double bil1, bil2, hasil;
                bil1=Double.valueOf(et1.getText().toString().trim());
                bil2=Double.valueOf(et2.getText().toString().trim());
                hasil=bil1/bil2;
                String hasill=String.valueOf(hasil);
                tvHasil.setText(hasill);

                String bill1=String.valueOf(bil1);
                String bill2=String.valueOf(bil2);

                Intent intent = new Intent (view.getContext(), MainActivity2.class);
                intent.putExtra("bil1", bill1);
                intent.putExtra("bil2", bill2);
                intent.putExtra("hasil", hasill);
                intent.putExtra("operasi", "/");
                startActivity(intent);
            }
        });
    }
}